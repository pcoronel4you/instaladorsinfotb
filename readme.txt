- Los artefactos contenidos en el instalador son los siguientes:

sinfotb.ear
Es la actualización del sistema SINFO-TB, se lo debe desplegar o copiar sobre el directorio: 

<directroriobase>/jboss-5.1.0.GA/server/sinfotb/deploy 
<directoriobase>es la carpeta dónde está instalado el servidor de aplicaciones jboss.

Previo a realizar este despliegue se debe detener la ejecución del servidor de aplicaciones.

jobs.sql  
Se ha agregado un script para agregar datos de ocupaciones a la BDD instalada en servidor del MSP.

Una vez ejecutado el script jobs.sql, se debe reiniciar la ejecución del servidor de aplicaciones, para que los cambios tomen efecto.




