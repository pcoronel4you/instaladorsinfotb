 update Treatment set censadosMayores5=0,                           
 censadosMenores5=0,                        
 conDiagnosticoTBMayores5=0,                  
 conDiagnosticoTBMenores5=0,                    
 examinadosMayores5=0,                          
 examinadosMenores5=0,                          
 ingresanTPIMenores5 =0,                        
 programadosMayores5 =0,                        
 programadosMenores5=0,                         
 terminanTPIMenores5=0;

update Treatment set glucemia=0;

ALTER TABLE Treatment MODIFY movilContacto VARCHAR(15);
ALTER TABLE Treatment MODIFY telefonoContacto VARCHAR(15);
ALTER TABLE Treatment MODIFY barrio VARCHAR(255);
ALTER TABLE Treatment MODIFY direccion VARCHAR(255);

